package com.qing.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthUserServiceApplication.class,args);
    }
}
